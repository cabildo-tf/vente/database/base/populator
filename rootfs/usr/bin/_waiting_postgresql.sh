#!/bin/bash

RETRIES=60
until [ ${RETRIES} -eq 0 ]
do
	if pg_isready -d ${POSTGRES_DB} -h ${POSTGRES_HOSTNAME} -p ${PORT_DB} -U ${POSTGRES_SDE_USERNAME}
	then
		echo "PostgreSQL is UP"
		break
	fi

 	echo -ne "."

	RETRIES=$((RETRIES-=1))
	sleep 1
done

if [ ${RETRIES} -eq 0 ]
then
	echo -e "\\nError connected to PostgreSQL!"
	exit 1
fi
