#!/bin/bash

function create_pgpass () {
	echo "${POSTGRES_HOSTNAME}:${PORT_DB}:*:${POSTGRES_SDE_USERNAME}:${POSTGRES_SDE_PASSWORD}" >> "${PGPASSFILE}"
	chmod 0600 ${PGPASSFILE}
}

function last_version () {
	local folder=$1

	last_version=$(ls -r $1 | head -n 1)
	echo "${BACKUPS_FOLDER}/${last_version}"
}


function restore () {
	local dump=$1

	echo "===================================================="
	if [ -z "$2" ]; then
		pg_restore -U ${POSTGRES_SDE_USERNAME} -d ${POSTGRES_DB} ${dump} -h ${POSTGRES_HOSTNAME}
		echo "Database restoring completed"
	else
		echo "Restore schema \"${schema}\""
		pg_restore -U ${POSTGRES_SDE_USERNAME} -d ${POSTGRES_DB} -n ${2} ${dump} -h ${POSTGRES_HOSTNAME}
		echo "Schema \"${schema}\" completed"
	fi
	echo "===================================================="
}


create_pgpass

dump=$(last_version ${BACKUPS_FOLDER})

echo "Restoring backup: ${dump}"

schemas=( "public" "sde" )
for schema in "${schemas[@]}"
do
	restore ${dump} ${schema}
done

restore ${dump}
